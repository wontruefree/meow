@[Link(ldflags: "#{__DIR__}/ext/meow.o")]
lib LibMEOW
  # BitWidth = LibMEOW.MeowHashSpecializeForCPU()

  # fun MeowHashSpecializeForCPU : Int32
  fun HashesAreEqual(UInt64, UInt64) : Int32
  fun MeowHash(UInt64, UInt64, Void*, Void*) : Void
  # fun MeowHash(UInt64, UInt64, Void*) : UInt128
  ## TODO(Jack): implement
  # fun MeowU64From(UInt128) : UInt64
  # fun MeowU32From(UInt128) : UInt32
  # Meow128_AreEqual
  # Meow128_AESDEC
  # Meow128_AESDEC_Mem
  # Meow128_AESDEC_Finalize
  # Meow128_Zero
  # Meow128_ZeroState
  # Meow128_Set64x2
  # Meow128_Set64x2_State
end
