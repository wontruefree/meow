#include "lib/meow_hash/meow_intrinsics.h"
#include "lib/meow_hash/meow_hash.h"

int
HashesAreEqual(meow_u128 A, meow_u128 B) {
    return MeowHashesAreEqual(A, B);
}

void
MeowHash(meow_u64 seed, meow_u64 size, void * buffer, void * result){
  meow_u128 hash = MeowHash_Accelerated(seed, size, buffer);
  result = &hash;
  return;
}
