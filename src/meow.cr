require "./lib_meow.cr"

struct Meow
  property bytes : Bytes
  property seed : UInt64

  def initialize(file : File, @seed : (Int32 | UInt64) = 0_u64)
    @bytes = file.gets_to_end.to_slice
  end

  def initialize(str : String, @seed : (Int32 | UInt64) = 0_u64)
    @bytes = str.to_slice
  end

  def self.from_file(filename : String)
    new(File.new(filename))
  end

  def mhash
    hash = Bytes.new(8)
    LibMEOW.MeowHash(seed, bytes.size, bytes.@pointer.as(Void*), hash.@pointer.as(Void*))
    hash
  end

  def self.mhash(string : String, seed : (Int32 | UInt64) = 0_u64)
    self.mhash(string, seed.to_u64)
  end

  def self.mhash(bytes = Bytes, seed : (Int32 | UInt64) = 0_u64)
    self.mhash(bytes.size, bytes.@pointer, seed.to_u64)
  end

  def self.mhash(size : (Int32 | Uint64), pointer : Pointer, seed : (Int32 | UInt64) = 0_u64)
    LibMEOW.MeowHash(seed.to_u64, size.to_u64, pointer.as(Void*))
  end

  # def == (other : Meow)
  #   LibMEOW.HashesAreEqual(mhash, other.mhash)
  # end

  def to_s
    mhash
    # "%08X-%08X-%08X-%08X\n" % IO::ByteFormat::SystemEndian.decode(UInt8, mhash)
  end
end

# puts LibMEOW::BitWidth
puts Meow.new("YOLO").to_s
